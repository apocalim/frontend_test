let bebidasArr = [
    'limonada',
    'fanta',
    'cerveza',
    'kerosene',
    'gasolina',
    'chicharra',
    'pisco',
    'punto g',
    'ron',
    'leche',
    'quaker',
    'guarana'
];

let mostrarBebidaNombre = (code) => {
    let index = code.split('code')[1];
    let valor = bebidasArr[index];
    let msg =  (typeof valor === 'undefined') ? 'agua de jamaica' : valor;
    console.log(msg)
}

mostrarBebidaNombre('code134')
