//Crear una función que reciba 2 parámetros, los cuales serán requeridos (no deben ser undefined). Si falta alguno de los parámetros, se debe lanzar un mensaje de error.

let requerido = (tipo) => { throw new Error(`Es necesario ingresar un ${tipo}`); };
let saludar = (name = requerido('nombre'), apellido = requerido('apellido')) => { console.log(`Hola ${name} ${apellido}`) };
