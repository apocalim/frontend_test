
class Guerrero {
  constructor(nombreJugador, raza) {
    console.log(`Bienvenido ${nombreJugador} has escogido la raza: ${raza}`);
  }

  hablar() {
    console.log('hablar');
  }

  gritar() {
    console.log('gritar');
  }
}

class Protoss extends Guerrero{
  correr() {
    console.log('correr');
  }
  defender() {
    console.log('defender');
  }
}

class Terran extends Guerrero{
  atacar() {
    console.log('atacar');
  }
  hechizar() {
    console.log('hechizar');
  }
}


class Zerg extends Guerrero{
  controlMental() {
    console.log('control mental');
  }
  volar() {
    console.log('volar');
  }

}

class Custom1 extends Protoss {
  atacar() {
    console.log('atacar');
  }
}


class Custom2 extends Zerg {
  hechizar() {
    console.log('hechizar');
  }

  sanar() {
    console.log('sanar');
  }
}

const clases = {Protoss, Zerg, Terran, Custom1, Custom2};
class crearClase {
  constructor (claseRaza, propiedades) {
      return new clases[claseRaza](propiedades, claseRaza);
  }
}

let jugador1 = new crearClase('Protoss', 'Juan');
let jugador2 = new crearClase('Zerg', 'Marco');
let jugador3 = new crearClase('Terran', 'Ramon');
let jugador4 = new crearClase('Custom1', 'Pedro');
let jugador5 = new crearClase('Custom2', 'Maria');

console.log(jugador1, jugador2, jugador3, jugador4, jugador5)


