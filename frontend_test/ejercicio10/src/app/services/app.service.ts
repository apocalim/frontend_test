import { Injectable } from '@angular/core';
import { ServiceModel } from '../models/service.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class AppService {

  private services: ServiceModel[];
  private servicesUpdated = new Subject<ServiceModel[]>();
  private validRoutes: Array<string> = ['autos', 'salud', 'hogar'];

  constructor (private http: HttpClient) {
  }

  getServices(typeView?: string) {
    this.http.get('./assets/data/services.json')
      .subscribe( (res: {servicios: ServiceModel[]}) => {
        this.services = res.servicios;
        this.servicesUpdated.next([...this.services]);
      });
  }

  isValidRoute(typeView) {
    return this.validRoutes.indexOf(typeView) !== -1;
  }

  getServicesUpdatedlistener() {
    return this.servicesUpdated.asObservable();
  }

  updateService(nameToUpdate: string, serviceToPut: ServiceModel) {
    const servicesC = [...this.services];
    servicesC.forEach( (service) => {
      if (service.name === nameToUpdate) {
        service.name = serviceToPut.name;
        service.desc = serviceToPut.desc;
        service.type = serviceToPut.type;
      }
    });
    this.services = servicesC;

    this.servicesUpdated.next([...this.services]);
  }

  deleteService(nameToDelete: string, typeParam: string) {
    const servicesC = [...this.services];
    const servicesNew = servicesC.filter( ({name, type}) => name !== nameToDelete);
    this.services = servicesNew;
    this.servicesUpdated.next([...this.services]);
  }

}
