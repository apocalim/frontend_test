import { Component, OnInit, HostBinding} from '@angular/core';
import { AppService } from '../../services/app.service';
import { ServiceModel } from '../../models/service.model';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html'
})

export class ServiceFormComponent implements OnInit {
  @HostBinding('class.service')
  service: ServiceModel;
  selectedName: string;

  constructor( private _appServices: AppService) { }


  ngOnInit() {
    this.fillForm({name: '', type: '', desc: ''});
  }

  updateService() {
    this._appServices.updateService(this.selectedName, this.service);
    this.cleanFrom();
  }

  fillForm(service: ServiceModel) {
    this.service = service;
    this.selectedName = service.name;
  }

  cleanFrom() {
    this.selectedName = '';
    Object.keys(this.service).filter((index) => {
      this.service.name = '';
      this.service.desc = '';
      this.service.type = '';
    });
  }

}
