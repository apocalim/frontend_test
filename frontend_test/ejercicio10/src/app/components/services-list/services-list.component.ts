import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { ServiceModel } from '../../models/service.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ServiceFormComponent } from '../service-form/service-form.component';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html'
})

export class ServiceListComponent implements OnInit, OnDestroy {

  services: ServiceModel[] = [];
  private serviceSubs: Subscription;
  private routeSubs: Subscription;
  public typeView: string;
  @Input() serviceForm: ServiceFormComponent;

  constructor( private _appServices: AppService,
    private _routerService: ActivatedRoute) { }


  ngOnInit() {
    this.routeSubs = this._routerService.paramMap.subscribe((paramMap: ParamMap) => {
      this.typeView = paramMap.get('type');
      this._appServices.getServices(this.typeView);
    });

    this.serviceSubs = this._appServices.getServicesUpdatedlistener()
      .subscribe( (services: ServiceModel[]) => {
        if (this.typeView !== undefined && this._appServices.isValidRoute(this.typeView)) {
          this.services = services.filter( ({type}) => type.toLocaleLowerCase() === this.typeView );
        } else {
          this.services = services;
        }
      });
  }

  ngOnDestroy() {
    this.routeSubs.unsubscribe();
    this.serviceSubs.unsubscribe();
  }

}
