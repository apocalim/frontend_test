import { Component, Input, HostListener } from '@angular/core';
import { ServiceModel } from '../../models/service.model';
import { AppService } from '../../services/app.service';
import { ServiceFormComponent } from '../service-form/service-form.component';


@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html'
})

export class ServiceCardComponent {

  @Input() service: ServiceModel;
  @Input() typeView: string;
  @Input() serviceForm: ServiceFormComponent;

  constructor(private _appServices: AppService) { }

  deleteService(name) {
    this._appServices.deleteService(name, this.typeView);
  }

  @HostListener('showService')
  showService() {
    this.serviceForm.fillForm({...this.service});
  }

}
