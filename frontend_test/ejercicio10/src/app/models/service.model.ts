export interface ServiceModel {
  name?: string;
  desc?: string;
  type?: string;
}
