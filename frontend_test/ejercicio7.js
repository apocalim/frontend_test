let personas = [
    {
        name: 'Pepe',
        donacion: true,
        esposas: ['Rosangela', 'Mayte', 'Nona']
    },
    {
        name: 'Juan',
        donacion: false,
        esposas: ['Yahaira']
    },
    {
        name: 'Lalo',
        donacion: true,
        esposas: []
    }
];

let personasRpta = personas.filter(({donacion, esposas}) => {
    if (donacion === true && esposas.length > 0) {
      let esposaRpta = esposas.find(esposa => {
          return (esposa.charAt(0) === 'Y' || esposa.charAt(0) === 'N');
      })
      return esposaRpta !== undefined
    }
  })
  .map( ({name}) => name );

console.log(personasRpta);
