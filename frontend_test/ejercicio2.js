//Escribir un programa que permita encontrar la suma acumulada de los cubos impares de los números enteros entre 1 y 1000.

let suma = () => {
    let i = 1;
    let arr = [];
    while (i <= 10) {
        arr.push(i);
        i++;
    }
    let numeros = [...arr];

    return numeros.filter( numero => numero % 2 !== 0)
        .map(numero => Math.pow(numero, 3))
        .reduce((a, b) => a + b, 0);
}
