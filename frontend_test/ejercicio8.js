let personas = [
    {
      name: 'Pepe',
      edad: 11
    },
    {
      name: 'Juan',
      edad: 22
    },
    {
      name: 'Lalo',
      edad: 33
    }
  ]

let excluirPersonas = (...args) => {
  let edades = [...args];
  edades.splice(0, 1);

  return personas.filter( ({edad}) =>  edades.indexOf(edad) === -1 )
    .map( ({name}) => name )
}

console.log(excluirPersonas(personas, 11, 12, 33));
